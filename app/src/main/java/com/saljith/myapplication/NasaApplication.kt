package com.saljith.myapplication
import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NasaApplication : Application()