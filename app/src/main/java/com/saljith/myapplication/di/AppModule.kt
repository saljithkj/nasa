

package com.saljith.myapplication.di
import android.app.Application
import androidx.room.Room
import com.saljith.myapplication.api.NASAApi
import com.saljith.myapplication.data.NasaDatabase

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(NASAApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideRestaurantApi(retrofit: Retrofit): NASAApi =
        retrofit.create(NASAApi::class.java)

    @Provides
    @Singleton
    fun provideDatabase(app: Application) : NasaDatabase =
        Room.databaseBuilder(app, NasaDatabase::class.java, "restaurant_database")
            .fallbackToDestructiveMigration()
            .build()
}