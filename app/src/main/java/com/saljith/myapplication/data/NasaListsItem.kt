package com.saljith.myapplication.data

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import javax.annotation.Nullable


@Parcelize
@Nullable
@Entity(tableName = "NasaListsItem")
data class NasaListsItem  (
    @PrimaryKey
    @NonNull
    val title: String,
    val copyright: String?,
    val date: String?,
    val explanation: String,
    val hdurl: String??,
    val media_type: String?,
    val service_version: String?,

    val url: String?
) : Parcelable