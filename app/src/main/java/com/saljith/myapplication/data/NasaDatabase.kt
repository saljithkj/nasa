
package com.saljith.myapplication.data
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [NasaListsItem::class], version = 7)
abstract class NasaDatabase : RoomDatabase() {

    abstract fun restaurantDao(): NasaDao
}