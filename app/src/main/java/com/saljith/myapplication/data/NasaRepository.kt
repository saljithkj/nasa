
package com.saljith.myapplication.data
import androidx.room.withTransaction
import com.saljith.myapplication.api.NASAApi
import com.saljith.myapplication.util.networkBoundResource

import kotlinx.coroutines.delay
import javax.inject.Inject

class NasaRepository @Inject constructor(
    private val api: NASAApi,
    private val db: NasaDatabase
) {
    private val restaurantDao = db.restaurantDao()

    fun getRestaurants() = networkBoundResource(
        query = {
            restaurantDao.getAllRestaurants()
        },
        fetch = {
            delay(1000)
            api.getNasaLists()
        },
        saveFetchResult = { restaurants ->
            db.withTransaction {
                restaurantDao.deleteAllRestaurants()
                restaurantDao.insertRestaurants(restaurants)
            }
        }
    )
}