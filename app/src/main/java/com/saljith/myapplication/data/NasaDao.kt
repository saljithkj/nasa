
package com.saljith.myapplication.data
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import kotlinx.coroutines.flow.Flow

@Dao
interface NasaDao {

    @Query("SELECT * FROM NasaListsItem")
    fun getAllRestaurants(): Flow<List<NasaListsItem>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRestaurants(restaurants: List<NasaListsItem>)

    @Query("DELETE FROM NasaListsItem")
    suspend fun deleteAllRestaurants()
}