
package com.saljith.myapplication.api

import com.saljith.myapplication.data.NasaLists
import retrofit2.http.GET

interface NASAApi {

    companion object {
        const val BASE_URL = "https://api.nasa.gov/planetary/"
    }

    @GET("apod?api_key=FnRf8Tm3zpTCyf1OtJ0XAd7bYDgAH0BIba0jGOxu&count=10")
    suspend fun getNasaLists(): NasaLists
}