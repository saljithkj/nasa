
package com.saljith.myapplication.ui
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.saljith.myapplication.databinding.ActivityNasaBinding

import com.saljith.myapplication.util.Resource

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NasaActivity : AppCompatActivity() {

    private val viewModel: NasaViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityNasaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val restaurantAdapter = NasaAdapter()

        binding.apply {
            recyclerView.apply {
                adapter = restaurantAdapter
                layoutManager = LinearLayoutManager(this@NasaActivity)

                (adapter as NasaAdapter).onItemClick= { contact ->

                    val intent = Intent(this@NasaActivity, NasaDetailActivity::class.java)
                    intent.putExtra("item", contact)
                    startActivity(intent)


                }


            }

            viewModel.restaurants.observe(this@NasaActivity) { result ->
                Log.e( "onCreate: ", result.data?.size.toString() )
                restaurantAdapter.submitList(result.data)

                progressBar.isVisible = result is Resource.Loading && result.data.isNullOrEmpty()
                textViewError.isVisible = result is Resource.Error && result.data.isNullOrEmpty()
                result.error?.localizedMessage?.let { Log.e( "onCreate: ", it) }
                textViewError.text = result.error?.localizedMessage
            }
        }



    }
}