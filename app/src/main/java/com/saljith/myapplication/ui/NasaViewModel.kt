
package com.saljith.myapplication.ui
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.saljith.myapplication.data.NasaRepository

import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class NasaViewModel @Inject constructor(
    repository: NasaRepository
) : ViewModel() {

    val restaurants = repository.getRestaurants().asLiveData()
}