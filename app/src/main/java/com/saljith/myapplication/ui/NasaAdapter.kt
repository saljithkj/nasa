package com.saljith.myapplication.ui



import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.saljith.myapplication.data.NasaListsItem
import com.saljith.myapplication.databinding.NasaItemBinding


class NasaAdapter() : ListAdapter<NasaListsItem, NasaAdapter.RestaurantViewHolder>(NASAComparator()) {
    var onItemClick: ((NasaListsItem) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        val binding =
            NasaItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RestaurantViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        val currentItem = getItem(position)
        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

   inner class RestaurantViewHolder(private val binding: NasaItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(restaurant: NasaListsItem) {
            binding.apply {
                Glide.with(itemView)
                    .load(restaurant.url)
                    .into(imageViewLogo)
//
                tvName.text = restaurant.title
                tvDate.text = restaurant.date

            }
            itemView.setOnClickListener {
                onItemClick?.invoke(restaurant)
            }


        }

    }

    class NASAComparator : DiffUtil.ItemCallback<NasaListsItem>() {
        override fun areItemsTheSame(oldItem: NasaListsItem, newItem: NasaListsItem) =
            oldItem.title == newItem.title

        override fun areContentsTheSame(oldItem: NasaListsItem, newItem: NasaListsItem) =
            oldItem == newItem
    }
}