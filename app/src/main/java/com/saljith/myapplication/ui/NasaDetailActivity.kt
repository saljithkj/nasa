package com.saljith.myapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.saljith.myapplication.R
import com.saljith.myapplication.data.NasaListsItem
import com.saljith.myapplication.databinding.ActivityNasaDetailBinding

class NasaDetailActivity : AppCompatActivity() {

    private lateinit var nasaitem: NasaListsItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityNasaDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)


        nasaitem = intent.getParcelableExtra("item")!!

        Glide.with(this)
            .load(nasaitem.url)
            .into(binding.ImageView)

        binding.tvName.setText(nasaitem.title)
        binding.tvDate.setText(nasaitem.date)
        binding.tvDes.setText(nasaitem.explanation)



        binding.back.setOnClickListener {

            super.onBackPressed()
        }




    }
}